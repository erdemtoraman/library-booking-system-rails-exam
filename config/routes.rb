Rails.application.routes.draw do
  get '/users/:id/loans', to: 'loan#show', as: 'loans'
  post '/extend/:id', to: 'loan#extend', as: 'extend'
end
