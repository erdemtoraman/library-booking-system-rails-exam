require 'rspec'
require 'rails_helper'
require 'rspec/core'

RSpec.describe LoanController, :type => [:controller, :request] do

  describe 'Querying for plants' do
    before(:each) do
      @user = User.create!({:username => 'usernameX'})
      loans = [{:book => "The Ruby way", :start_date => "09-01-2016", :return_date => "16-01-2016", :times_extended => 0},
               {:book => "Javascript", :start_date => "09-01-2016", :return_date => "16-01-2016", :times_extended => 0},
      ]
      loans.each { |loan|
        book= Book.create!({:title => loan[:book], :call_no => Random.new(100).to_s})
        Loan.create!({:user => @user, :book=> book, :start_date => loan[:start_date], :return_date => loan[:return_date], :times_extended => loan[:times_extended]})
      }
    end

    it 'should render correct page' do
        get loans_path(@user)
        expect(response).to render_template :loans_index
    end


  end
end
