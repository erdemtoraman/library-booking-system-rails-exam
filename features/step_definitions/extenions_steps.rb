
Given(/^I am logged in with the username "([^"]*)"$/) do |username|
  @user = User.create!({:username => username})
end

Given(/^I am on the loans page$/) do
    visit "users/#{@user.id}/loans"
end

Given(/^I have the following loans:$/) do |table|
  # table is a Cucumber::MultilineArgument::DataTable
  table.hashes.each do |loan|
    book= Book.create!({:title => loan[:book], :call_no => Random.new(100).to_s})
    Loan.create!({:user => @user, :book=> book, :start_date => loan[:start_date], :return_date => loan[:return_date]})
  end
end

Given(/^the book "([^"]*)" "([^"]*)" reserved$/) do |book_title, is_reserved|
  is_reserved = is_reserved.include?('not') ? false : true
   Book.find_by_title(book_title).update!(:is_reserved => is_reserved)
end

Given(/^I click in extend for the book "([^"]*)"$/) do |book_title|
  book = Book.find_by_title(book_title)
  loan = Loan.find_by_book_id(book.id)
  click_link("extend#{loan.id}")
end

Then(/^I should see "([^"]*)"$/) do |content|
  if page.respond_to? :should
    page.should have_content(content)
  else
    assert page.has_content?(content)
  end
end

