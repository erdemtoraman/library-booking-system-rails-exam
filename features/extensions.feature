Feature: Extension for a book
  In order to extend a loan
  As a user
  I should be able to request extension and see the output

  Scenario: Listing my loans
    Given I am logged in with the username "erdem"
    Given I have the following loans:
      | book   | start_date | return_date   | times_extended |
      | The Ruby way| 09-01-2016 |16-01-2016   |   0      |
      | Javascript | 09-01-2016   | 16-01-2016|   0      |
    Given the book "The Ruby way" "is not" reserved
    Given the book "Javascript" "is" reserved
    Given I am on the loans page
    Then I should see "Javascript"
    Then I should see "The Ruby way"


  Scenario: Being able to extend
    Given I am logged in with the username "erdem"
    Given I have the following loans:
      | book   | start_date | return_date   | times_extended |
      | The Ruby way| 09-01-2016 |16-01-2016   |   0      |
      | Javascript | 09-01-2016   | 16-01-2016|   0      |
    Given the book "The Ruby way" "is not" reserved
    Given the book "Javascript" "is" reserved
    Given I am on the loans page
    And I click in extend for the book "The Ruby way"
    Then I should see "Extension Accepted"
