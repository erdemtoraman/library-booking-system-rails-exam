# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

@user = User.create!({:username => 'usernameX'})
loans = [{:book => "The Ruby way", :start_date => "09-01-2016", :return_date => "16-01-2016", :times_extended => 0},
         {:book => "Javascript", :start_date => "09-01-2016", :return_date => "16-01-2016", :times_extended => 0},
]
loans.each { |loan|
  book= Book.create!({:title => loan[:book], :call_no => Random.new(100).to_s})
  Loan.create!({:user => @user, :book=> book, :start_date => loan[:start_date], :return_date => loan[:return_date], :times_extended => loan[:times_extended]})
}