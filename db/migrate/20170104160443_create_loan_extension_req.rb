class CreateLoanExtensionReq < ActiveRecord::Migration
  def change
    create_table :loan_extension_reqs do |t|
      t.timestamp :return_date
      t.string :status
    end
  end
end
