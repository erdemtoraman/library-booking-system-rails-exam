class CreateLoan < ActiveRecord::Migration
  def change
    create_table :loans do |t|
      t.timestamp :start_date
      t.timestamp :return_date
      t.float :cost
      t.integer :times_extended
    end
  end
end
