class CreateBook < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.string :call_no
      t.string :title
      t.string :authors
      t.boolean :is_reserved
    end
  end
end
